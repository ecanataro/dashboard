# Imagen base
FROM ubuntu

# Actualiza el sistema e instala Apache
RUN apt update && apt install -y apache2

# Copia los archivos HTML y la configuración de Apache
COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf

# Expone los puertos del contenedor
EXPOSE 80 443

# Define el comando que se ejecutará al iniciar el contenedor
CMD ["apache2ctl", "-D", "FOREGROUND"]
