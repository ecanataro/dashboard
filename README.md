# Dockerfile

Nombre y apellido: Emanuel Federico Canataro
Materia: Infraestructura en la Nube
Curso: 73041-2024-1C-Infraestructuras
División: A-Día jueves
Profesor: Sergio Pernas

## Instrucciones

- Crear un repositorio.

- Crear una imagen Docker.

- Documentar todas las intrucciones de creacion de imagen, despliegue, archivos configuración, etc. en el REAME.md.

- Agregar archivo de configuracion de ejemplo para balanceador/proxy reverso.

- Presentar el enlace al repositorio en el TP.

## Introducción

Se escoje el dashboard como proyecto, este contará con dos servidores docker y un nginx como balanceador. Inicialmente, se mostrará cómo realizar la creación del proyecto en Gitlab.

## Requisitos

### Entorno de ejecución para replicación

- Oracle VM VirtualBox Versión 7.0.18 r162988 (Qt5.15.2)

### Sistema operativo y red

- Servidor Ubuntu 22.04.4 LTS (GNU/Linux 5.15.0-112-generic x86_64) x 3
- Red NAT: 200.0.0.0/24
- IP Docker 1: 200.0.0.50
- IP Docker 2: 200.0.0.51
- IP Nginx: 200.0.0.52

## Desarrrollo

#### Paso 1:

Crear una cuenta en https://gitlab.com/
Luego de hacerlo será necesario crear un nuevo proyecto en el siguiente enlace: https://gitlab.com/projects/new#blank_project
En este caso, al haber elegido el dashboard utilizaremos "dashboard" como nombre de proyecto, esto generará una url que combinará el sitio de gitlab, nuestro nombre de usuario y el nombre del proyecto. En mi caso el enlace generado es el siguiente: https://gitlab.com/ecanataro/dashboard
Para el proyecto debemos elegir un nivel de visibilidad público para permitir su acceso al profesor, y optamos por la creación del mismo con un README inicial.

#### Paso 2:

En Virtualbox debemos acceder a Herramientas, Red y dentro de Redes NAT se deberá elegir para Crear. Luego se hará click derecho en el nuevo elemento y Propiedades. Allí se podrá establecer un nombre que identifique a la red NAT del laboratorio, un prefijo IPv4 propio y asignar el reenvío de puertos.

Se configuró con los siguientes valores:
Nombre: VPC
IPv4: 200.0.0.0/24
Reenvío de puertos:
HTTP Docker 1 TCP 0.0.0.0 8080  200.0.0.50  80
HTTP Docker 2 TCP 0.0.0.0 8081  200.0.0.51  80
SSH Docker 1  TCP 0.0.0.0 5022  200.0.0.50  22
SSH Docker 2  TCP 0.0.0.0 5023  200.0.0.51  22
SSH Ngnix     TCP 0.0.0.0 5024  200.0.0.52  22

#### Paso 3:

Generar una vm en VirtualBox con la imagen de Ubuntu 22. Los pasos de este procedimiento han sido explicados en anteriores trabajos que se pueden tomar de referencia. Se utiliza una imagen que posee configurados algunos aspectos básicos como un usuario, su contraseña, etc.

#### Paso 4:

Configurar un hostname que identifique su uso. En este caso será Docker1, lo haremos con el siguiente comando:
`# hostnamectl set-hostname Docker1`

#### Paso 5:

Ahora modificaremos la dirección ip propia del servidor con el siguiente comando:
`# nano /etc/netplan/00-installer-config.yaml`

Se modifican los valores para establecer el mencionado en los requisitos y se guarda:
network:
    version: 2
    renderer: networkd
    ethernets:
        enp0s3:
            addresses:
                - 200.0.0.50/24
            nameservers:
                addresses: [8.8.8.8, 8.8.4.4]
            routes:
                - to: default
                  via: 200.0.0.2
Luego se asegura la correcta aplicación con el siguiente comando:
`# netplan apply`

#### Paso 6:

Procederemos a la creación de una clave SSH, para esto nos posicionaremos en la ruta ssh con el siguiente comando:
`# cd .ssh/`
Podemos revisar si existe alguna con un ls, sin embargo en nuestro caso no se cuenta con una, por lo que utilizaremos este comando para crearla:
`# ssh-keygen`
Se nos solicitará el directorio y contraseña (dos veces), como realizamos esto con fines de práctica estos valores serían vacíos y simplemente presionaremos enter ante su pedido.

#### Paso 7 (optativo):

Habiendo configurado la ip y teniendo el redireccionamiento de forma correcta podremos conectar por PowerShell a través de SSH, permitiéndonos una mejor manipulación del servidor. Lo haremos de la siguiente manera:
`ssh -p 5022 istea@localhost`
Se nos solicitará la contraseña asociada, y luego de ingresarla correctamente si es la primera vez que accedemos tendremos que confirmar que deseamos acceder.

#### Paso 8:

Utilizaremos el siguiente comando para extraer la clave pública:
`# cat .ssh/id_rsa.pub`
Debemos copiar lo que nos devuelva.

#### Paso 9:

Accederemos a https://gitlab.com/-/user_settings/ssh_keys y presionaremos en Add new key. Pegaremos el valor copiado en Key y le daremos un nombre identificador de referencia. También se puede escoger una fecha de expiración de esta clave, al ser para un laboratorio no nos interesa que sea persistente así que elegiremos una expiración corta. Deberemos presionar en New key.

#### Paso 10:

Instalaremos docker de la siguiente manera:
`# apt install docker docker.io`
Si nos solicitara confirmación lo haríamos para permitir que continúe el proceso.

#### Paso 11:

Añadiremos el usuario al grupo de Docker con el siguiente comando:
`# gpasswd -a istea Docker`

#### Paso 12:

Accederemos a la siguiente ruta: /home/istea
`# cd /home/istea`
E insertaremos el siguiente comando para clonar el repositorio creado anteriormente en Gitlab:
`# git clone git@gitlab.com:ecanataro/dashboard.git`
Nos solicitará confirmar luego de que nuestra clave pública brinde el acceso.
Nota: Cada uno tendrá un código diferente en base a su usuario y nombre de proyecto.

#### Paso 13:

Nos moveremos a la ruta /home/istea/dashboard para trabajar desde aquí en adelante:
`# cd /dashboard/`

#### Paso 14:

Configuramos el usuario que realizará el push:
`# git config user.name "John Doe"`
`# git config user.email john@doe.com`

#### Paso 15 (optativo):

Verificaremos la rama/branch en la que estamos trabajando actualmente:
`# git branch`

#### Paso 16:

Descargaremos el paquete de la app Dashboard:
`# wget https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-examples.zip`

#### Paso 17:

Al haber descargado un zip, necesitaremos ser capaces de descomprimir este formato, instalaremos lo necesario:
`# apt install unzip`
Extraeremos y moveremos los archivos necesarios creando el directorio html:
`# unzip bootstrap-5.3.3-examples.zip`
`# mv bootstrap-5.3.3-examples/dashboard html`
`# mv bootstrap-5.3.3-examples/assets html`

#### Paso 18:

Removemos lo que ya no resulta necesario:
`# rm bootstrap-5.3.3-examples.zip -r`
`# rm -r bootstrap-5.3.3-examples*`
Y generaremos el primer commit y push:
`# git add .`
`# git commit -m "dashboard-app`
`# git push origin main`
Nota: Si visualizamos nuestro proyecto en el sitio web de Gitlab visualizaremos los cambios.

#### Paso 19:

Crearemos una imagen Dockerfile:
`# nano Dockerfile`
Y le añadiremos el siguiente contenido:
FROM ubuntu
RUN apt update && apt install -y apache2
COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf
EXPOSE 80 443
CMD ["apache2ctl", "-D", "FOREGROUND"]

Luego de guardarlo, utilizaremos el siguiente comando:
`docker build -t dashboard-image .`

#### Paso 20:

Aprovechamos la generosidad del profesor y utilizamos su versión de apache2.conf optimizada:
`# wget https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/apache2.conf `

#### Paso 21:

Lanzamos el contenedor con el siguiente comando:
`# docker run -d --name dashboard -p 8080:80 dashboard-image`
Y comprobamos su ejecución con el siguiente comando:
`# docker ps`

#### Paso 22:

Utilizamos el comando curl para asegurar de que el docker funciona del modo esperado:
`curl localhost:8080`
Se nos devuelve el código del sitio web respectivo. ¡Es un éxito!

#### Paso 23:

Luego de asegurar su funcionamiento nos aseguraremos de guardar el progreso en el repositorio:
`# git add .`
`# git commit -m "dashboard-app`
`# git push origin main`

#### Paso 24:

En Virtualbox se clona la vm Docker1 para generar el Docker2.
Dentro de ella se generan los ajustes necesarios para su uso/diferenciación:
`# nano /etc/netplan/00-installer-config.yaml`
Habiendo modificado por la ip planificada (200.0.0.51) se asegura su aplicación
`# netplan apply`
Se cambia el hostname al correcto:
`# hostnamectl set-hostname Docker2`

#### Paso 25

Se limpian los rastros de la instancia Docker y se ejecuta una nueva, respetando el puerto de redirección asignado:
`# docker system prune`
`# docker run -d --name dashboard -p 8081:80 dashboard-image`

#### Paso 26:

Utilizamos el comando curl para asegurar de que el docker funciona del modo esperado:
`curl localhost:8081`
Se nos devuelve el código del sitio web respectivo. ¡Es otro éxito!

#### Paso 27:

Se genera una nueva vm en Virtualbox con una imagen limpia, esta servirá de Nginx.
Dentro de ella se generan los ajustes necesarios para su uso/diferenciación:
`# nano /etc/netplan/00-installer-config.yaml`
Habiendo modificado por la ip planificada (200.0.0.52) se asegura su aplicación
`# netplan apply`
Se cambia el hostname al correcto:
`# hostnamectl set-hostname Nginx`

#### Paso 28:

Se le instala la aplicación objetivo y se activa:
`# apt install nginx`
`# systemctl enable nginx`
Se verifica su estado con el siguiente comando para asegurar que esté Active (running):
`# systemctl status nginx`

#### Paso 29:

Nos movemos a /etc/nginx/sites-available
`# cd /etc/nginx/sites-available`
Y configuraremos el vhost nginx y balanceo:
`#nano dashboard.net.conf`
E ingresaremos los siguientes valores (estos corresponden a nuestros servidores, a las ips brindadas, puertos, etc):
upstream docker-backends {
        server 200.0.0.50:8080;
        server 200.0.0.51:8081;
        least_conn;
}
server {
        listen 80;
        server_name dashboard.net;
        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass http://docker-backends/;
        }
}
Luego guardaremos con Ctrl+X

#### Paso 30:

Creamos un enlace simbólico de la configuración generada a /etc/nginx/sites-enabled/:
`# ln -s /etc/nginx/sites-available/dashboard.net.conf /etc/nginx/sites-enabled/`

#### Paso 31:

Recargamos el servicio para que apliquen las configuraciones que realizamos:
`# systemctl reload nginx.service`

## Conclusión

A pesar de ser un proceso largo y que requiere especial atención a los detalles, podemos (si es que seguimos los pasos indicados) obtener un sitio web que alternará en round robin (gracias a nginx) entre ambos dashboards generados en los servidores Docker1 y Docker2.

## Fuentes
https://drive.google.com/drive/folders/1peuWym94nB97j2EbMexlNcL0_3tgSUcu
https://docs.gitlab.com/ee/user/index.html
https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/apache2.conf
https://support.typora.io/Markdown-Reference/

## Acceso al README
https://gitlab.com/ecanataro/dashboard/-/blob/main/README.md
